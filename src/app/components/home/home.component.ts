import {Component, OnDestroy, OnInit} from '@angular/core';
import {ResourceService} from '../../services/resource/resource.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

    private numbersSubscription: Subscription;
    numbers: number[] = [];

    constructor(private resourceService: ResourceService) {
    }

    ngOnInit() {
        this.resourceService.getNumbers().subscribe(numbers => {
            this.numbers = numbers;
        });
    }

    ngOnDestroy() {
        this.numbersSubscription && this.numbersSubscription.unsubscribe();
    }

}
