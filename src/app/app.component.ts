import {Component, OnInit} from '@angular/core';
import {PlatformService} from './services/platform/platform.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'ng-static';

    constructor(private platformService: PlatformService) {

    }

    ngOnInit() {
    }

}
