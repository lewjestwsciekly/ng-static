// These are important and needed before anything else
import 'zone.js/dist/zone-node';
import 'reflect-metadata';

import {enableProdMode} from '@angular/core';

import * as express from 'express';
import {join} from 'path';
// Express Engine
import {ngExpressEngine} from '@nguniversal/express-engine';
// Import module map for lazy loading
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';
import {serverRouter} from './server-router';
import {CONFIG} from './config';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

class Server {
    private app: express.Application;

    constructor() {

// Express server
        const app = this.app = express();

        const DIST_FOLDER = join(process.cwd(), 'dist');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
        const setEngine = () => {
            const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('../../dist/server/main');
            app.engine('html', ngExpressEngine({
                bootstrap: AppServerModuleNgFactory,
                providers: [
                    provideModuleMap(LAZY_MODULE_MAP)
                ],
            }));
            app.set('view engine', 'html');
            app.set('views', join(DIST_FOLDER, 'browser'));
        };
        setEngine();

// TODO: implement data requests securely
        app.use('/api', serverRouter);

// Server static files from /browser
        app.get(['*.js', '*.js.map', '*.css', '*.css.map', '*.ico', 'assets/*.*'], (req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.locals.staticNotFound = true;
            return express.static(join(DIST_FOLDER, 'browser'))(req, res, next);
        });

// All regular routes use the Universal engine
        app.get('*', (req, res: express.Response) => {
            if (res.locals.staticNotFound) {
                return res.sendStatus(404);
            }
            res.render('index', {req});
        });
    }

    run() {
        const app = this.app;
        const PORT = CONFIG.PORT || 4000;

        return new Promise(resolve => {
// Start up the Node server
            app.listen(PORT, () => {
                resolve(PORT);
            });
        });
    }
}

export const server = new Server();
