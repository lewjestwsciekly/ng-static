import * as express from 'express';

export const serverRouter = express.Router();

serverRouter.get('/*', (req: express.Request, res: express.Response) => {
    const data = [];
    const l = Math.ceil(Math.random() * 10) + 2;
    for (let i = 0; i < l; i++) {
        data.push(Math.random());
    }
    res.status(200).json(data);
});
